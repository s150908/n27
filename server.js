class Konto {
    constructor() {
        this.Kontonummer
        this.Kontoart
        this.Iban
    }
}

// Klassendefinition.
class Kunde {
    constructor() {
        this.Nachname
        this.Vorname
        this.IdKunde
        this.Geschlecht
        this.Geburtsdatum
        this.Adresse
        this.Kennwort
    }
}

// Deklaration und Instanziierung.
let kunde = new Kunde()
// Initialisierung.
kunde.IdKunde = 4711
kunde.Kennwort = "123"
this.Nachname = "Schmidt"
this.Vorname = "Nele"
this.Geschlecht = "weiblich"
this.Geburtsdatum = "2001-11-19"
this.Adresse = "Berlin"

const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const iban = require('iban')
const app = express()
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(cookieParser())

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})
// Ausgabe von "Server lauscht..." im Terminal


// Die app.get('/'...) wird abgearbeitet, wenn die Startseite im Browser aufgerufen wird.
app.get('/',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)
        res.render('index.ejs', {                              
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

// Wenn die Seite localhost:3000/impressum aufgerufen wird, ...

app.get('/impressum',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)
        
        // ... dann wird impressum.ejs gerendert.
        
        res.render('impressum', {                              
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

app.get('/login',(req, res, next) => {         
    res.cookie('istAngemeldetAls', '')       
    res.render('login.ejs', {                    
    })
})

app.post('/',(req, res, next) => {   
    
    // Der Wert des Inputs mit dem name = "idkunde" wird über den Request zugewiesen, an die Konstante idKunde
    const idKunde = req.body.idKunde
    const kennwort = req.body.kennwort
    console.log(idKunde + " == " + kunde.IdKunde + "&&" + kennwort + " == " + kunde.Kennwort)
        
    // Wenn der Wert von idKunde dem Wert der der Eigenschaft kunde.IdKunde entspricht und der Wert von kennwort der Eigenschaft kunde.Kennwort entspricht, dann werden die Anweisungen im Rumpf der if-Kontrollstruktur abegearbeitet. 
    if(idKunde == kunde.IdKunde && kennwort == kunde.Kennwort){            
        console.log("Der Cookie wird gesetzt:")
        res.cookie('istAngemeldetAls', idKunde)
        res.render('index.ejs', {           
        })
    }else{            
        console.log("Der Cookie wird gelöscht")
        res.cookie('istAngemeldetAls','')
        res.render('login.ejs', {                    
        })
    }
})
app.get('/kontoAnlegen',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)
        
        // ... dann wird impressum.ejs gerendert.
        
        res.render('kontoAnlegen.ejs', {
            meldung : ""                              
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

app.post('/kontoAnlegen',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)
        let konto = new Konto()
        konto.Kontonummer = req.body.kontonummer
        konto.Kontoart = req.body.kontoart 
        const bankleitzahl = 27000000
        const laendererkennung = "DE"
        konto.Iban = iban.fromBBAN(laendererkennung,bankleitzahl + " "+ konto.Kontonummer)

        //Der Wert aus dem input mit dem Namen "kontonummer" 
        //wird zugewiesen (=) an die Eigenschaft Kontonummer
        //des Objekts namens konto.
        
        
        

        
        // ... dann wird impressum.ejs gerendert.
        
        res.render('kontoAnlegen', {
            meldung : "Das " + konto.Kontoart +  " mit der Kontonummer " + konto.Kontonummer +  "  wurde erfolgreich angelegt."                              
        })
    }else{
       //Die login.ejs wird gerendert und als response an den Browser übergeben.
        res.render('login.ejs', {                    
        })    
    }
})


app.post('/stammdatenPflegen',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Die Stammdaten von " +IdKunde + " wurden erfolgreich geändert.")
        
        kunde.Nachname = req.body.nachname
        kunde.Vorname = req.body.vorname 
        
        
        res.render('stammdatenPflegen', {
            meldung : "Die Stammdaten von " +kunde.IdKunde + " wurden erfolgreich geändert."                             
        })
    }else{
      
        res.render('stammdatenPflegen.ejs', {   
            meldung: "Die Stammdaten von " +kunde.IdKunde + " wurden erfolgreich geändert."                 
        })    
    }
})

app.get('/stammdatenPflegen',(req, res, next) => {         
    res.cookie('istAngemeldetAls', '')       
    res.render('stammdatenPflegen.ejs', {  
        meldung: ""                  
    })
})