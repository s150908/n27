const express = require('express')
const bodyParser = require('body-parser')
const app = express()
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended: true}))
app.set('views', 'Training')

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})

// Eine Klasse ist ein Bauplan. Der Bauplan sieht vor, wie Objekte erstellt werden.
// Alle Objekte, die von einem Bauplan erstellt werden, haben die selben Eigenschaften, 
// aber möglicherweise unterschiedliche Eigenschaftswerte.

// Klassendefinition
// =================

class Rechteck {
    constructor() {
        this.laenge
        this.breite
    }
}

// Klassendefinition für Schüler in einer Schule: 

class Schueler {
    constructor() {
        this.geschlecht
        this.klasse
        this.alter
        this.vorname
        this.nachname
    }
}

// Autoverwaltung

class Auto  {
    constructor(){
        this.sitze
        this.marke
    }
}

// Fußballerverwaltung

class Fussballer {
    constructor(){
        this.vorname
        this.nachname
        this.mannschaft
        this.geschlecht
    }
}

// Deklaration eines neuen Objekts vom Typ Rechteck
// Deklaration = Bekanntmachung
// let rechteck = ...

// Instanziierung eines neuen Objekts 
// Instanziierung erkennt man immer am reservierten Wort "new"
// Bei der Instanziierung wird Arbeitsspeicher bereitgestellt.
// ... = new Rechteck()

// 1. Deklaration  2. Instanziierung 
let rechteck = new Rechteck()

let schueler = new Schueler()

let auto = new Auto()

let fussballer = new Fussballer()

// 3. Initialisierung (Konkrete Eigenschaftswerte werden zugewiesen)
rechteck.breite = 2
rechteck.laenge = 3

console.log("Länge:" + rechteck.laenge)

console.log("Breite:" + rechteck.breite)

schueler.geschlecht = "w"
schueler.alter = 17

console.log("Geschlecht:" + schueler.geschlecht)

console.log("Alter:" + schueler.alter)

auto.sitze = 2
auto.marke = "Opel"

console.log("Sitze:" + auto.sitze)

console.log("Marke:" + auto.marke)

fussballer.mannschaft = "FC Borken"
fussballer.vorname = "Firke"

// Wenn localhost:3000/klasse-objekt-ejs-trainieren aufgerufen wird ...

app.get('/klasse-objekt-ejs-trainieren',(req, res, next) => {   

    // ... wird klasse-objekt-ejs-trainieren.ejs gerendert:

    res.render('klasse-objekt-ejs-trainieren', {    
        breite : rechteck.breite,
        laenge : rechteck.laenge,
        geschlecht : schueler.geschlecht,
        alter : schueler.alter,
        sitze: auto.sitze,
        marke: auto.marke,
        fussballer: fussballer.mannschaft,
        fussballer: fussballer.vorname.       
    })
})


